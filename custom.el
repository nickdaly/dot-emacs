(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(async-bytecomp-allowed-packages '(\'\(all\)))
 '(bmkp-last-as-first-bookmark-file "~/.emacs.d/bookmarks")
 '(bookmark-save-flag 1)
 '(column-number-mode t)
 '(custom-enabled-themes '(wombat))
 '(custom-safe-themes
   '("76c5b2592c62f6b48923c00f97f74bcb7ddb741618283bdb2be35f3c0e1030e3" default))
 '(darkroom-margins-if-failed-guess 0.07)
 '(desktop-restore-eager 5)
 '(desktop-save-mode nil)
 '(display-battery-mode t)
 '(display-buffer-alist '(("\\*mail\\*" display-buffer-no-window)))
 '(display-time-mode t)
 '(doc-view-ghostscript-options
   '("-dSAFER" "-dNOPAUSE" "-sDEVICE=png16m" "-dTextAlphaBits=4" "-dBATCH" "-dGraphicsAlphaBits=4" "-dQUIET" "-sICCProfilesDir=/usr/share/color/icc/ghostscript/"))
 '(doc-view-resolution 300)
 '(ediff-custom-diff-options "-uw")
 '(electric-pair-mode t)
 '(erc-autojoin-channels-alist
   '(("libera.chat" "#fossandcrafts" "#haskell" "#python")
     ("oftc.net" "#debian" "#freedombox")))
 '(erc-autojoin-delay 10)
 '(erc-autojoin-mode t)
 '(erc-autojoin-timing 'connect)
 '(erc-hide-list '("JOIN" "PART" "QUIT"))
 '(erc-hide-timestamps t)
 '(erc-modules
   '(autojoin button completion irccontrols list match menu move-to-prompt netsplit networks noncommands readonly ring track truncate))
 '(erc-paranoid t)
 '(erc-prompt-for-password nil)
 '(erc-server "irc.libera.chat")
 '(erc-timestamp-only-if-changed-flag t)
 '(erc-truncate-mode t)
 '(erc-user-mode "+Q")
 '(geiser-default-implementation 'guile)
 '(global-auto-revert-mode t)
 '(global-undo-tree-mode t)
 '(gnus-select-method '(nntp "news.aioe.org"))
 '(gnus-summary-expunge-below 0)
 '(ido-mode 'both nil (ido))
 '(indent-tabs-mode nil)
 '(inhibit-startup-screen t)
 '(initial-scratch-message nil)
 '(ispell-silently-savep t)
 '(menu-bar-mode nil)
 '(nntp-authinfo-file "~/.authinfo.gpg")
 '(notmuch-draft-folder "Drafts")
 '(notmuch-hello-refresh-hook '(nmd/offline-imap))
 '(notmuch-saved-searches
   '((:name "unread-to-me" :query "to:despisinggravity and tag:unread")
     (:name "inbox" :query "tag:inbox" :key "i")
     (:name "unread" :query "tag:unread" :key "u")
     (:name "flagged" :query "tag:flagged" :key "f")
     (:name "sent" :query "tag:sent" :key "t")
     (:name "drafts" :query "tag:draft" :key "d")
     (:name "all mail" :query "not tag:deleted" :key "a")))
 '(notmuch-show-empty-saved-searches nil)
 '(nyan-animate-nyancat nil)
 '(nyan-wavy-trail t)
 '(org-agenda-files '("~/.org/todo.org" "~/.org/notes.org"))
 '(org-babel-load-languages
   '((emacs-lisp . t)
     (plantuml . t)
     (dot . t)
     (python . t)
     (shell . t)
     (haskell . t)
     (R . t)
     (maxima . t)
     (scheme . t)))
 '(org-babel-plantuml-svg-text-to-path t)
 '(org-babel-python-command "python3")
 '(org-catch-invisible-edits 'show-and-error)
 '(org-confirm-babel-evaluate 'nmd/org-confirm-babel-evaluate)
 '(org-default-notes-file "~/.org/todo.org")
 '(org-directory "~/.org")
 '(org-ditaa-jar-path "/usr/share/ditaa/ditaa.jar")
 '(org-file-apps
   '((auto-mode . emacs)
     (directory . emacs)
     ("\\.mm\\'" . default)
     ("\\.x?html?\\'" . default)
     ("\\.pdf\\'" . "evince %s")))
 '(org-latex-pdf-process '("latexmk -shell-escape -bibtex -f -pdf %f"))
 '(org-log-done 'time)
 '(org-modules
   '(org-bbdb org-bibtex org-docview org-eww org-gnus org-info org-irc org-mhe org-rmail org-w3m org-ref org-pdfview))
 '(org-plantuml-jar-path "/usr/share/plantuml/plantuml.jar")
 '(org-ref-insert-cite-key "C-c C-x l")
 '(org-todo-keywords '((sequence "TODO" "WAIT" "|" "DONE" "CANCELLED")))
 '(org-use-property-inheritance '("noweb-ref" "noweb"))
 '(package-archives
   '(("gnu" . "https://elpa.gnu.org/packages/")
     ("melpa" . "http://melpa.org/packages/")))
 '(package-selected-packages
   '(wanderlust writeroom-mode ox-epub gemini-mode ox-gemini elpher map xr zenburn-theme "zenburn-theme-melpa" "zenburn-theme" org-re-reveal-ref "org-ref" "org-ref" zone-nyan nyan-mode adaptive-wrap go-mode org-pdfview interleave darkroom rainbow-delimiters org-bullets org imenu-list htmlize haskell-mode graphviz-dot-mode ess))
 '(pdf-annot-default-annotation-properties
   '((t
      (label . "Nick Daly")
      (flags . 4))
     (text
      (color . "#ff0000")
      (icon . "Note"))
     (highlight
      (color . "yellow"))
     (underline
      (color . "blue"))
     (squiggly
      (color . "orange"))
     (strike-out
      (color . "red"))))
 '(pdf-info-epdfinfo-program "/usr/bin/epdfinfo")
 '(pdf-tools-enabled-modes
   '(pdf-history-minor-mode pdf-isearch-minor-mode pdf-links-minor-mode pdf-misc-minor-mode pdf-outline-minor-mode pdf-misc-size-indication-minor-mode pdf-misc-menu-bar-minor-mode pdf-annot-minor-mode pdf-sync-minor-mode pdf-misc-context-menu-minor-mode pdf-cache-prefetch-minor-mode pdf-view-auto-slice-minor-mode pdf-occur-global-minor-mode))
 '(plantuml-default-exec-mode 'jar)
 '(plantuml-jar-path "/usr/share/plantuml/plantuml.jar")
 '(python-shell-interpreter "python3")
 '(read-mail-command 'ignore)
 '(ring-bell-function 'ignore)
 '(safe-local-variable-values
   '((eval add-to-list 'nmd/org-babel-evaluate-okay-files "~/.emacs.d/init.org")
     (eval add-hook 'after-save-hook
           '(lambda nil
              (run-at-time
               (- 60
                  (mod
                   (floor
                    (float-time))
                   60))
               nil
               '(lambda nil
                  (compile "make -C ~/blog -k all"))))
           nil t)
     (eval add-hook 'after-save-hook
           '(lambda nil
              (compile "make -C ~/blog -k all"))
           nil t)
     (eval add-hook 'after-save-hook
           '(lambda nil
              (compile "make -k all"))
           nil t)
     (auto-revert-mode . 1)
     (org-babel-exp-code-template . "/=<<%name>>=/ =
#+begin_export latex
\\begin{framed}
#+end_export
#+BEGIN_SRC %lang%switches%flags
%body
#+END_SRC
#+begin_export latex
\\end{framed}
#+end_export")
     (org-babel-exp-code-template . "/=<<%name>>=/ =
#+begin_export latex
\\begin{framed}
#+end_export
#+BEGIN_SRC %lang%switches%flags
%body
#+END_SRC
#+begin_export latex
\\end{framed}
\\captionof{figure}{\\label{%name}%caption}
#+end_export")
     (eval require 'org-ref)
     (eval add-hook 'after-save-hook
           '(lambda nil
              (compile "make -k"))
           nil t)
     (eval add-hook 'after-save-hook 'nmd-org-init nil t)
     (eval add-hook 'after-save-hook
           '(lambda nil
              (compile "make -kC .. clean all"))
           nil t)
     (eval add-hook 'after-save-hook
           '(lambda nil
              (compile "make -kC .."))
           nil t)))
 '(scroll-bar-mode nil)
 '(show-paren-mode t)
 '(speedbar-sort-tags t)
 '(speedbar-supported-extension-expressions
   '(".hs" ".org" ".[ch]\\(\\+\\+\\|pp\\|c\\|h\\|xx\\)?" ".tex\\(i\\(nfo\\)?\\)?" ".el" ".emacs" ".l" ".lsp" ".p" ".java" ".js" ".f\\(90\\|77\\|or\\)?" ".ad[abs]" ".p[lm]" ".tcl" ".m" ".scm" ".pm" ".py" ".g" ".s?html" ".ma?k" "[Mm]akefile\\(\\.in\\)?"))
 '(tags-revert-without-query t)
 '(text-mode-hook
   '(turn-on-flyspell visual-line-mode text-mode-hook-identify))
 '(tool-bar-mode nil)
 '(vc-follow-symlinks nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :extend nil :stipple nil :background "#242424" :foreground "#f6f3e8" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 158 :width normal :foundry "UNKW" :family "Iosevka"))))
 '(font-lock-comment-face ((t (:foreground "#99968b" :family "DejaVu Sans Mono"))))
 '(org-block ((t (:inherit fixed-pitch))))
 '(org-code ((t (:inherit (shadow fixed-pitch)))))
 '(org-document-info ((t (:foreground "dark orange"))))
 '(org-document-info-keyword ((t (:inherit (shadow fixed-pitch)))))
 '(org-indent ((t (:inherit (org-hide fixed-pitch)))))
 '(org-link ((t (:inherit link :family "DejaVu Sans Mono"))))
 '(org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)))))
 '(org-property-value ((t (:inherit fixed-pitch))) t)
 '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
 '(org-table ((t (:inherit fixed-pitch))))
 '(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold :height 0.8))))
 '(org-verbatim ((t (:inherit (shadow fixed-pitch))))))
